from date import Date

if __name__ == '__main__':
    headers = {
        'User-Agent': 'Mozilla/5.0'
    }
    date = Date(headers=headers)
    print(date.get_miladi_date())
