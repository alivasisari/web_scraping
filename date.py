from bs4 import BeautifulSoup
import requests


class Date:
    """
    A class used to represent current date
    ...
    Attributes
    -----

    url : str
        url of site that have a date

    headers : dictionary
        attrs for send to url

    request : requests.get
        for request to url

    soup : BeautifulSoup
        get html from response of request and pars it

    shamsi_class , ghamari_class ,miladi_class: str
        html div class of shamsi , ghamari , miladi date

    Methods
    -----

    get_shamsi_date()
        return shamsi date

    get_ghamari_date()
        return ghamari date

    get_miladi_date()
        return miladi date

    """


    url = 'https://www.bahesab.ir/time/'
    shamsi_class = "date-11"
    ghamari_class = "date-33"
    miladi_class = "date-22"

    def __init__(self, headers):
        """
        :param headers:
            get header information from user

        ...

        send request to url with headers
        get response of request
        with BeautifulSoup get request content and parse it

        """
        self.headers = headers
        self.request = requests.get(self.url, headers=headers)
        self.soup = BeautifulSoup(self.request.content, "html.parser")

    def __get_date(self, class_name):
        """
        :param class_name:
            get div class name that have date
        :return:
            return today date
        """
        return self.soup.find(attrs={"class": class_name}).contents[0]

    def get_shamsi_date(self):
        """
        :return:
            return today date in shamsi from html content from request response
        """
        return self.__get_date(self.shamsi_class)

    def get_miladi_date(self):
        """
        :return:
            return today date in miladi  from html content from request response
        """
        return self.__get_date(self.miladi_class)

    def get_ghamari_date(self):
        """
        :return:
            return today date in ghamari from html content from request response
        """
        return self.__get_date(self.ghamari_class)
